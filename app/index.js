/**
 * Application entry point
 */
import 'babel-polyfill';

// Load application styles
import 'styles/index.scss';
import 'styles/stylesheet.scss';

// ================================
// START YOUR APP HERE
// ================================
import Flow from './flow';

// flowText.style.display = "none";

document.onreadystatechange = function () {
  if (document.readyState == "complete") {
    let preview = true;
    let url = new URL(window.location);
    let params = new URLSearchParams(url.search);
    for(var pair of params.entries()) {
      if(pair[0] === "preview") {
        preview = (pair[1] === "true");
      }
    }

    let pages = document.querySelector(".pages");
    let scale = ((window.innerWidth * .9 ) / pages.offsetWidth);

    pages.style.transform = `scale(${scale}) translate(${(window.innerWidth / 2) - ((pages.offsetWidth * scale / 2) ) }px, 0)`;


    let flowText = document.querySelector("#flow");

    let t0 = performance.now();

    let flow = new Flow(flowText.content, preview).then((flow) => {
      let t1 = performance.now();

      console.log("Rendering " + flow.total + " pages took " + (t1 - t0) + " milliseconds.");
    });

  }
};
