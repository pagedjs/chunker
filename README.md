Paged Media Chunker
===========

Chunks up a document into paged media flows and applies print styles.

Process the first 50 pages of Moby Dick: [https://s3.amazonaws.com/pagedmedia/chunker/index.html](https://s3.amazonaws.com/pagedmedia/chunker/index.html).

## Setup
Install dependencies
```sh
$ npm install
```

## Development
Run the local webpack-dev-server with livereload and autocompile on [http://localhost:8080/](http://localhost:8080/)
```sh
$ npm start
```
## Deployment
Build the current application
```sh
$ npm run build
```
